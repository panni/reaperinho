#!/usr/bin/env python2.7
# coding=utf-8

import traceback
import warnings
import os
import sys
import threading
import pystray
import signal
import time
import argparse
import win32gui
import win32con
import platform
import subprocess
import logging

from pystray import Menu, MenuItem
from pystray._util.win32 import WM_LBUTTONDBLCLK
from pywinauto import ElementNotFoundError, win32defines, MatchError, ElementAmbiguousError, win32functions
from pywinauto.application import Application, process_module, WindowSpecification
from pywinauto.controls import InvalidWindowHandle
from PIL import Image
from pywinauto.timings import TimeoutError
from pywinauto.findwindows import find_windows

from exc import WrongConfiguration, AppEnded, StopWatching, CantRun, MultipleInstancesFound
from config import Config
from named_mutex import NamedMutex

log = logging.getLogger('reaperinho')
log.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
filelog = logging.FileHandler("reaperinho.log", mode="w", encoding="utf-8")
filelog.setFormatter(formatter)
filelog.setLevel(logging.DEBUG)
log.addHandler(console)
log.addHandler(filelog)

STATES = {
    win32defines.SW_SHOWMINIMIZED: "minimized",
    win32defines.SW_NORMAL: "active",
}

VERSION = "0.0.9b"
VERSION_STRING = 'REAPERinho v%s' % VERSION
DETACHED_PROCESS = 0x00000008
CREATE_NEW_PROCESS_GROUP = 0x00000200


class Reaper(object):
    path = None
    start_delay = 0
    auto_minimize = False
    check_interval = 1000
    wait_for_window = "REAPERmixervu"
    move_fix = True
    run_apps = []
    only_run_apps = False

    app = None
    dlg = None
    last_state = None
    icon = None
    image = None
    image_d = None

    _kill_app = False
    _loaded = False
    _watching = False
    _stop_watching = False
    _handle = None
    _last_window_position = None
    _changing_visibility = False
    _first_start = True

    _fill_args = ("path", "start_delay", "auto_minimize", "check_interval", "wait_for_window", "move_fix", "run_apps",
                  "only_run_apps")

    def __init__(self):
        self.config = Config()

    def configure(self, args):
        """
        Initialize class with args, fall back to specified config if possible
        :param args: argparse args
        :return:
        """

        # load config from file
        if args.config:
            self.config.from_file(args.config)
            log.info(u"Using config: %s" % args.config.name)

        # fill values from config or script args
        for arg in self._fill_args:
            value = getattr(args, arg)
            default_for_arg = parser.get_default(arg)

            # if the given argument is a default one, try fetching it from config file, else use it
            if value == default_for_arg:
                if arg in self.config:
                    value = self.config[arg]

            # check for app path existence
            if arg == "path" and value is not None and not os.path.isfile(value):
                raise WrongConfiguration(u"The path \"%s\" doesn't exist" % value)

            # clamp check_interval to >= 100ms
            elif arg == "check_interval" and value < 100:
                raise WrongConfiguration("Minimum check_interval is 100ms")

            elif arg == "start_delay" and not getattr(args, "startup", False):
                continue

            elif arg == "run_apps":
                if getattr(args, arg) and arg in self.config and self.config[arg]:
                    value = self.config[arg]
                else:
                    continue

            setattr(self, arg, value)

    @property
    def title(self):
        """
        :return: Title based on state
        """
        if self.app:
            if not self._loaded:
                return "REAPER started, waiting for load"
            else:
                return "REAPER %s, PID: %d" % (STATES.get(self.last_state, "running"), int(self.app.process))
        return "idle, waiting for load"

    def update_state_info(self, update_menu=True):
        """
        Updates the icon, title and the menu based on state
        :return:
        """
        self.icon.icon = self.image if self._loaded else self.image_d
        self.icon.title = self.title
        if update_menu:
            self.icon.update_menu()

    def first_window(self, **criteria):
        handles = find_windows(**dict(process=self.app.process, **criteria))
        if handles:
            return WindowSpecification(dict(handle=handles[0]))

        raise ElementNotFoundError

    @property
    def loaded(self):
        """
        :return: True if app is running, else False
        """
        if not self.app:
            return False

        try:
            self._loaded = self.first_window(class_name=self.wait_for_window, top_level_only=False).is_visible()
        except (ElementNotFoundError, MatchError):
            self._loaded = False

        return self._loaded

    def run_app(self, ignore_delay=False):
        """
        Runs the app specified by self.path. self.path is either set when running the script or after REAPER has been
        found.
        :type ignore_delay: True/False
        :return:
        """
        if not self.path:
            return

        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            if self.start_delay and not ignore_delay:
                log.info("Waiting for %ds" % self.start_delay)
                time.sleep(self.start_delay)

            log.info(u"Running REAPER: %s" % self.path)
            Application().start(self.path)

    def kill_app(self):
        """
        Kills the app by sending Alt+F4 to its main window.
        :return:
        """
        window_to_close = None
        if self.dlg and self.dlg.Exists():
            window_to_close = self.dlg

        elif self.app:
            window_to_close = self.app.window()

        if window_to_close:
            log.info("Closing REAPER")
            self.toggle_visibility(action="show")
            window_to_close.close(wait_time=60)
            self.app.wait_for_process_exit()

        self.reset()

    def check(self, update_menu=True, display_message=True):
        """
        Checks whether the app is running.
        :return:
        """
        if display_message:
            log.info("Checking for REAPER existence")

        self.app = None

        try:
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                app = Application().connect(class_name="REAPERwnd")
                app.REAPERwnd.wait("exists")
        except ElementNotFoundError:
            self.update_state_info()
            return False
        except ElementAmbiguousError:
            raise MultipleInstancesFound

        log.info("REAPER app found: %d" % int(app.process))

        self.app = app
        if not self.path:
            self.path = process_module(int(self.app.process))

        self.update_state_info(update_menu=update_menu)

        return True

    def reset(self, skip_state_info=False):
        """
        Resets the internal attributes to default.
        :return:
        """
        self.app = None
        self.dlg = None
        self._handle = None
        self._loaded = False
        self._kill_app = False
        self.last_state = None
        self._last_window_position = None

        if not skip_state_info:
            self.update_state_info()

    def wait(self, display_message=True):
        """
        Waits for the app and for the necessary controls to become ready.
        Raises StopWatching() if stop was requested and auto minimizes the main window if requested.
        :return:
        """
        if not self.app:
            log.info("Waiting for REAPER to start")

        first_try = True
        while not self.app:
            if self._stop_watching:
                break

            self.check(update_menu=False, display_message=first_try)
            first_try = False
            time.sleep(0.5)

        if self._stop_watching:
            self._watching = False
            raise StopWatching()

        if display_message:
            log.info("Waiting for REAPER to become ready")

        try:
            self.app.REAPERwnd.wait('exists', timeout=5.0)
            dlg = self.app.REAPERwnd
            handle = dlg.handle

        except MatchError:
            log.error("Match Error")
            return
        except TimeoutError:
            log.error("Timed out")
            self.app = None
            self.dlg = None
            self.run_app()
            return self.wait(display_message=False)

        try:
            self.app.REAPERwnd.wait('ready')
            self.first_window(class_name=self.wait_for_window, top_level_only=False).wait("ready", timeout=1.0)
            log.info("REAPER ready")

        except (MatchError, ElementNotFoundError):
            return
        except TimeoutError:
            return self.wait(display_message=False)

        if not self.dlg:
            self.dlg = dlg
            self._handle = handle

        self._loaded = True
        if self.auto_minimize:
            self.toggle_visibility(action="hide")
        self.update_state_info()

        if self._first_start:
            self._first_start = False

            if self.run_apps:
                log.info(u"First start, run startup applications")
                self._run_apps()

    def _run_apps(self):
        for path in self.run_apps:
            try:
                log.info(u"Running application: %s" % path)
                if sys.platform == "win32":
                    if not path[0].endswith(".lnk"):
                        with open(os.devnull, 'wbN') as DEVNULL:
                            subprocess.Popen(path, shell=False, stdin=subprocess.PIPE,
                                             stdout=DEVNULL, stderr=DEVNULL,
                                             creationflags=DETACHED_PROCESS | CREATE_NEW_PROCESS_GROUP,
                                             env=os.environ)
                    else:
                        # special case for shortcuts (chrome seems to need this for starting with profile dir)
                        cmd = path[0] if path[0].startswith('"') else '"%s"' % path[0]
                        os.startfile(cmd)
                else:
                    subprocess.Popen(path, shell=False, stdin=None, stdout=None, stderr=None,
                                     close_fds=True)

            except:
                log.error(u"Couldn't start application: %s" % path)
                traceback.print_exc()

    def toggle_visibility(self, action=None):
        """
        Toggles the visibility of the app's main window. Either based on the last known window state, or forced by
        specifying the action parameter.
        :param action: string, "hide" or "show"
        :return:
        """
        if not self.last_state and action is None:
            return

        if not self.dlg:
            return

        if self._changing_visibility:
            return

        self._changing_visibility = True

        try:
            is_shown_normally = self.dlg.GetShowState() == win32defines.SW_SHOWNORMAL
            if not action:
                action = "hide" if is_shown_normally else "show"

            if action == "hide":
                log.info("Hiding REAPER")
                # windows 7 fallback
                move_window = False
                if self.move_fix and is_shown_normally:
                    self._last_window_position = pos = self.dlg.rectangle()
                    move_window = True

                self.dlg.HideFromTaskbar()
                self.dlg.minimize()

                if move_window:
                    log.debug("DEBUG moving window")
                    self.dlg.MoveWindow(-1000, -1000)

                self.last_state = win32defines.SW_SHOWMINIMIZED
            elif action == "show":
                log.debug("Showing REAPER")
                win32functions.ShowWindow(self._handle, win32defines.SW_HIDE)
                win32functions.SetWindowLongPtr(self._handle, win32defines.GWL_EXSTYLE, 65808)
                win32functions.ShowWindow(self._handle, win32defines.SW_SHOW)

                if self.move_fix and self._last_window_position:
                    self.dlg.MoveWindow(None, self._last_window_position.top)

                self.dlg.restore()
                self.last_state = win32defines.SW_SHOWNORMAL

            self.update_state_info()
        finally:
            self._changing_visibility = False

    def close(self):
        """
        Closes the tray icon. If self._kill_app is set, also kills the app.
        Called internally by the observer thread.
        :return:
        """
        if self.app:
            if self._kill_app:
                self.kill_app()
            else:
                self.toggle_visibility(action="show")

        log.info("Closing myself")
        self.reset(skip_state_info=True)
        self.icon.visible = False
        self.icon.stop()
        os._exit(0)

    def stop(self, kill=False):
        """
        Instructs the observer thread to stop watching, the whole script to end, and optionally kills the app as well.
        :param kill: True/False
        :return:
        """
        self._kill_app = kill
        self._stop_watching = True

    def observe(self):
        """
        Waits for the app to load, then enters a busy loop watching for changes to the app's main window state.
        Toggles the visibility of the app accordingly (when minimized it removes the main window from the taskbar).
        :return:
        """
        def has_state_changed():
            if self._stop_watching:
                raise StopWatching()

            # fixme: this may be an unnecessary duplicate
            if not self.dlg.Exists(timeout=0.1):
                raise AppEnded()

            dlg_state = self.dlg.GetShowState()
            if self.last_state:
                if self.last_state != win32defines.SW_SHOWMINIMIZED and dlg_state == win32defines.SW_SHOWMINIMIZED:
                    self.last_state = win32defines.SW_SHOWMINIMIZED
                    return True

            self.last_state = dlg_state

        try:
            self.wait()
        except StopWatching:
            self._watching = False
            self.close()
            return

        self._watching = True
        timer = 0

        log.info("Watching REAPER for state changes ...")

        while 1:
            if self._stop_watching:
                self._watching = False
                self.close()
                break

            # check state every self.check_interval ms
            if timer and timer % self.check_interval is 0:
                timer = 0
                try:
                    if not self.dlg or not self.dlg.Exists(timeout=0.1):
                        raise AppEnded()

                    if has_state_changed() and self.last_state == win32defines.SW_SHOWMINIMIZED:
                        self.toggle_visibility(action="hide")

                except (InvalidWindowHandle, AppEnded, StopWatching, MatchError), e:
                    self.reset()

                    # app ended involuntarily
                    if not isinstance(e, StopWatching):
                        log.info("REAPER exited")
                        try:
                            self.wait()
                        except StopWatching:
                            self._watching = False
                            self.close()
                            return
                    else:
                        # app ended because we're exiting, break loop
                        self._watching = False
                        self.close()
                        return

            timer += 100
            time.sleep(0.1)


reaper = Reaper()
parser = argparse.ArgumentParser()


def manage_reaper():
    log.info("REAPERinho initialized")
    if not reaper.only_run_apps:
        try:
            if not reaper.check(update_menu=False):
                reaper.run_app()

        except CantRun, e:
            if isinstance(e, MultipleInstancesFound):
                log.info("Multiple REAPER instances found, exiting")
            _close()
            return

        log.info("Starting state monitoring")
        state_watcher = threading.Thread(target=reaper.observe)
        state_watcher.run()
    elif reaper.only_run_apps and reaper.run_apps:
        reaper._run_apps()
        reaper.close()


def _close(kill=False):
    reaper.stop(kill=kill)


def click_close(*args):
    _close()


def click_close_kill(*args):
    _close(kill=True)


def stub(*args):
    return


def toggle_reaper_visibility(*args):
    reaper.toggle_visibility()


def stop_reaper(*args):
    reaper.kill_app()


def start_reaper(*args):
    reaper.run_app(ignore_delay=True)


def restart_reaper(*args):
    if reaper.loaded:
        stop_reaper(*args)

    start_reaper(*args)


def is_reaper_running(*args):
    return reaper.loaded


def have_reaper_path_and_not_running(*args):
    return bool(reaper.path) and not reaper.loaded


def state_menu_item(*args):
    return 'State: %s' % reaper.title


def setup_systray():
    log.info("--------------------")
    log.info(VERSION_STRING)
    log.info("--------------------")

    parser.add_argument('path', nargs='?', default=None, help="The path to reaper.exe; if not given, the app will "
                                                              "wait for REAPER to run")
    parser.add_argument('-a', '--auto_minimize', action="store_true", default=False,
                        help="Automatically minimize REAPER to system tray when REAPER is detected")
    parser.add_argument('-r', '--run_apps', action="store_true", default=False,
                        help="Run applications from \"run_apps\" config entry after REAPER successfully started, once.")
    parser.add_argument('-o', '--only_run_apps', action="store_true", default=False,
                        help="Only run applications from \"run_apps\" config entry and exit, without starting REAPER.")
    parser.add_argument('-d', '--start_delay', type=int, default=0,
                        help="Wait for given amount of seconds before starting REAPER. Useful for Windows-boot.")
    parser.add_argument('--startup', action="store_true", default=False,
                        help="Enabled the use of start_delay.")
    parser.add_argument('-i', '--check_interval', type=int, default=Reaper.check_interval,
                        help="How often to check for state changes in the app, in milliseconds. Default: %dms" %
                             Reaper.check_interval)
    parser.add_argument('-w', '--wait_for_window', type=str, default=Reaper.wait_for_window,
                        help="Wait for window class. Default: %s (wait for non-master-tracks to load). "
                             "Use \"REAPERvu\" to wait for the master track to load." %
                             Reaper.wait_for_window)
    parser.add_argument('-m', '--move_fix', action="store_true", default=True,
                        help="Explicitly move window after minimizing")
    parser.add_argument('-c', '--config', type=file, default=None,
                        help="Specify config file. Default if existing: config.json. Arguments to the script always"
                             "override the config's equivalent.")

    try:
        args = parser.parse_args()

        if not args.config:
            fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), "config.json")
            if os.path.isfile(fn):
                args.config = open(fn, "r")

    except SystemExit:
        return

    reaper.configure(args)

    reaper.image = Image.open("1.ico")
    reaper.image_d = Image.open("1d.ico")
    reaper.icon = pystray.Icon('REAPERinho', title=VERSION_STRING, icon=reaper.image_d, menu=Menu(
        MenuItem('REAPERinho v%s' % VERSION, stub, enabled=False),
        Menu.SEPARATOR,
        MenuItem(state_menu_item, stub, enabled=False),
        Menu.SEPARATOR,
        MenuItem('Toggle REAPER visibility', toggle_reaper_visibility, default=True, visible=is_reaper_running),
        MenuItem('Close REAPER', stop_reaper, visible=is_reaper_running),
        MenuItem('Run REAPER', start_reaper, visible=have_reaper_path_and_not_running),
        MenuItem('Restart REAPER', restart_reaper, visible=is_reaper_running),
        Menu.SEPARATOR,
        MenuItem('Close REAPERinho', click_close),
        MenuItem('Close', click_close_kill),
    ))

    reaper.icon.ICON_CLICK_TYPE = WM_LBUTTONDBLCLK

    def setup(icon):
        icon.visible = True
        try:
            manage_reaper()
        except KeyboardInterrupt:
            log.info("exiting")
            _close()
        except:
            log.exception("something went wrong: ")
            os._exit(0)

    log.debug("Setting up tray icon")
    reaper.icon.run(setup)


def signal_handler(signal, frame):
    _close()


signal.signal(signal.SIGINT, signal_handler)

if __name__ == "__main__":
    exit_duplicate = False
    try:
        with NamedMutex("REAPERinho", timeout=0) as mutex:
            if mutex.acquired:
                setup_systray()
            else:
                exit_duplicate = True
    except WindowsError:
        exit_duplicate = True
    except WrongConfiguration:
        log.exception(u"Bad configuration: %s")

    if exit_duplicate:
        log.error("Only one REAPERinho instance is allowed to run at the same time, exiting.")
