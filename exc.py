# coding=utf-8


class StopWatching(Exception):
    pass


class AppEnded(Exception):
    pass


class CantRun(Exception):
    pass


class MultipleInstancesFound(CantRun):
    pass


class WrongConfiguration(Exception):
    pass
