# coding=utf-8

import json


class Config(dict):
    def from_file(self, f):
        data = json.load(f, encoding="utf-8")
        self.update(data)

    def __getattr__(self, item):
        return self.get(item, None)
